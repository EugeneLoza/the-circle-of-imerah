{ Load Screen displayer

  Copyright (c) 2019 Yevhen Loza

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
}

{ --------------------------------------------------------------------------- }

(* Load and show an image before the game loads completely
   Call ShowLoadScreen(Window) after the Window is open
   and call FreeLoadScreen after the load sequence is complete *)

unit LoadScreen;

{$mode objfpc}{$H+}

interface

uses
  CastleWindow;

{initialize and show Load screen}
procedure ShowLoadScreen(const aWindow: TCastleWindowCustom; const aImageURL: String);
{free memory used by Load screen}
procedure FreeLoadScreen;
implementation
uses
  CastleGLImages;

var
  LoadScreenImage: TGLImage;
  ThisWindow: TCastleWindowCustom;

procedure RenderLoadScreen(Container: TUIContainer);
begin
  LoadScreenImage.Draw(0, 0, Container.Width, Container.Height);
end;

procedure ShowLoadScreen(const aWindow: TCastleWindowCustom; const aImageURL: String);
begin
  LoadScreenImage := TGLImage.Create(aImageURL);
  ThisWindow := aWindow;
  ThisWindow.OnRender := @RenderLoadScreen;
  ThisWindow.Invalidate;
  Application.ProcessAllMessages;
end;

procedure FreeLoadScreen;
begin
  ThisWindow.OnRender := nil;
  LoadScreenImage.Free;
end;

end.

