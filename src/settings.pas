{ Copyright (C) 2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* Read/Write game configuration/settings *)

unit Settings;

{$INCLUDE compilerconfig.inc}

interface

{ Save images solution statistics and configuration to disk }
procedure SaveConfig;
{ Load images solution statistics and configuration from disk }
procedure LoadConfig;

implementation
uses
  CastleConfig,
  DecoLog, SoundMusic;

procedure SaveConfig;
begin
  Log(CurrentRoutine, 'Saving configruation...');
  UserConfig.SetValue('$SOUND_ON', PlaySound);
  UserConfig.Save;
end;

procedure LoadConfig;
var
  i: Integer;
begin
  Log(CurrentRoutine, 'Reading configruation...');
  UserConfig.Load;
  FPlaySound := UserConfig.GetValue('$SOUND_ON', true);
end;

end.

