{ Copyright (C) 2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit Game;

{$INCLUDE compilerconfig.inc}

interface

uses
  WindowUnit;

procedure NewGame;

procedure InitGame;
procedure FreeGame;
implementation
uses
  CastleKeysMouse,
  CastleWindow, CastleGLImages;

var
  Image: TGLImage;

procedure GameRender(Container: TUIContainer);
begin

end;

procedure GamePress(Container: TUIContainer; const Event: TInputPressRelease);
begin

end;

procedure GameMotion(Container: TUIContainer; const Event: TInputMotion);
begin

end;

procedure GameRelease(Container: TUIContainer; const Event: TInputPressRelease);
begin

end;

procedure NewGame;
begin

end;

procedure InitGame;
begin
  Window.OnRender := @GameRender;
  Window.OnPress := @GamePress;
  Window.OnMotion := @GameMotion;
  Window.OnRelease := @GameRelease;
end;

procedure FreeGame;
begin
  Window.OnRender := nil;
  Window.OnPress := nil;
  Window.OnMotion := nil;
  Window.OnRelease := nil;
end;




end.

