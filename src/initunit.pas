{ Copyright (C) 2019 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

{ --------------------------------------------------------------------------- }

(* {} *)

unit InitUnit;

{$INCLUDE compilerconfig.inc}

interface

{uses
  Classes, SysUtils;}

implementation
uses
  SysUtils, CastleWindow, CastleApplicationProperties, CastleFilesUtils,
  Game,
  LoadScreen, Settings, SoundMusic,
  WindowUnit, DecoLog, Global;

function GetApplicationName: String;
begin
  Result := 'The Circle of Imerah';
end;

procedure ApplicationInitialize;
begin
  Log(CurrentRoutine, 'Init sequence started.');
  ShowLoadScreen(Window, ApplicationData('loadscreen.jpg'));
  sleep(3000);

  InitGame;

  FreeLoadScreen;

  Log(CurrentRoutine, 'Init sequence finished.');
end;

initialization
InitLog;
InitGlobal;
InitWindow;

//init CastleWindow.Application properties
Application.MainWindow := Window;
Application.OnInitialize := @ApplicationInitialize;

//set application name (three copies)
SysUtils.OnGetApplicationName := @GetApplicationName;
ApplicationProperties(true).ApplicationName := GetApplicationName;
Window.Caption := 'The Circle of Imerah';

finalization
FreeGame;
FreeLog;
FreeGlobal;

end.

