# The Circle of Imerah

A planned action/platformer game including an epic adventure of Helia and her friends sentinels in a magical realm of Imerah.

_A glare of light in the endless void, a wonderful world flourishes with life above desolate Aiter Sea. Guarded by the Sun and the Moon in perpetual circle of day and night, the circle of Imerah. This is my home. And this is my duty as a Sentinel to protect this world at any cost._

The game is made in Castle Game Engine (Lazarus/FreePascal), is free, libre and open source.

# Windows

No installation is required, just extract all files into a single folder and play.

# Linux

No installation is required, just extract all files into a single folder and play. You may need to set "executable" permission for SwappyJigsaw.run.

Libraries required for the game (Linux: Debian/Ubuntu package reference):

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* libgtkglext1
* libatk-adaptor (soft dependency of GTK2)
* You'll also need OpenGL drivers for your videocard. Usually it is libgl1-mesa-dev.
* You also need X-system and GTK at least version 2, however, you are very likely to have those already installed :)

# Links

Source code repository: https://gitlab.com/EugeneLoza/the-circle-of-imerah

Please, report any bugs, issues and ideas to: https://gitlab.com/EugeneLoza/the-circle-of-imerah/issues
