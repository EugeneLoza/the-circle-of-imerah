//is z-index set anywhere?
//draw flipped (for any object: ImageRect)
//No xy map! only collision objects at float heights

type
  { A simple object on map }
  TBatchedObject = class(TObject)
  strict protected
    { Internal time of this object, most often refers to animation or movement }
    FTime:Single;
    { Is this object on screen? }
    function isOnScreen: Boolean;
    { Set if other objets may collide with this one }
    procedure SetCollides(const aValue: Boolean);
    { Get on-screen width of this object }
    function GetWidth: Single;
    { Get on-screen height of this object }
    function GetHeight: Single;
  public
    { Bottom-left position of this object }
    x, y: Single;
    { Z-index of this object (drawing order) }
    zIndex: Integer;
    { Image to draw }
    Image: TGLImage; // doesn't own Image!
    { Call-back in case of collision }
    onCollision: TSimpleProcedure;
    { Can other objects collide with this one? }
    property Collides: Boolean read FCollides write SetCollides;
    { On-screen width of this object }
    property Width: Single read GetWidth;
    { On-screen height of this object }
    property Height: Single read GetHeight;
    { Update this object (called every frame) }
    procedure Update(const SecondsPassed: Single); virtual;
    { Add this object to the queue for batched rendering }
    procedure QueueRender; virtual;
    { Is (ax, ay) inside this object? }
    function IfPointInside(const ax, ay): Boolean;
    { Does this object collides with aRect? }
    function IfCollides(const aRect: TFloatRectangle): Boolean;
  public
    { Rectangle in atlas, Rectangle to draw on screen and rectangle to collide }
    ImageRect, ScreenRect, CollisionRect:TFloatRectangleArray;
  end;
  
type
  { Object that can be animated }
  TAnimatedObject = class(TBatchedObject)
  strict private
    { Get rectangle in atlas for current animation frame }
    function GetAnimationFrame: TFloatRectangle;
  public
    procedure Update(const SecondsPassed: Single); override;
    procedure QueueRender; override;
  end;

type
  { Object that can be controlled to move }
  TDynamicObject = class(TAnimaredObject)
  strict protected
    { Speed vector }
    vx, vy: Single;
    { Is this object currently standing on something collidable? }
    function StandingOnTheGround: Boolean;
    { Get collision rectangle after adding avx and avy to coordinates }
    function NextRect(const avx, avy: Single): TFloatRectangle;
  public
    procedure Update(const SecondsPassed: Single); override;
    { Try jumping }
    procedure Jump;
    { Move left or right }
    procedure MoveMe(const DeltaX: Integer);
    { Receive incoming damage }
    //procedure HitMe(const Damage: Single);
  end;

type
  {}
  TSmartObject = class(TDynamicObject)
  strict protected
    {}
    //procedure PathFinder;
  end;

type
  { A creature with health }
  TCreature = class(TSmartObject)
  public
    {}
    HP: Single;
    {}
    procedure HitMe(const Damage: Single);
    {}
    procedure Die; vitrual; abstract;
  end;
  
type
  { Controllable character }
  TPlayerCharacter = class(TCreature)
  end;
  
type
  { (Sorted) list of batched objects }
  TBatchedObjectList = specialize TObjectList<TBatchedObject>;

type
  { Comparer to sort the list of batched objects }
  TBatchedComparer = specialize TComparer<TBatchedObjects>;
  
type
  { World of the game }
  TWorld = class(TObject)
  strict protected
    { Stand-alone objects in this world }
    FObjectList: TBatchedObjectList;
    { List containing objects in this world that can collide }
    FCollidersList: TBatchedObjectList;
    { Overall map of the world }
    FMap, FMapBackground, FMapForeground: array of array of integer;
    {}
    FMapAtlas: TGLImage;
    {}
    FMapAtlasRecords: array of TFloatRectanlge;
    {}
    procedure QueueRenderMap;
    {}
    function isValid(const ax, ay: Integer): Boolean; inline;
    {}
    function Map(const ax, ay: Integer): Integer; inline;
    {}
    function MapForeground(const ax, ay: Integer): Integer; inline;
    {}
    function MapBackground(const ax, ay: Integer): Integer; inline;
    {}
    function isPassable(const ax, ay: Integer): Boolean; inline;
  strict private
    { Requests rebuilding FCollidersList }
    FNeedToRebuildColliders: Boolean;
    { Rebuilds FCollidersList }
    procedure doRebuildColliders;
  public
    { Add an object to the world }
    procedure AddObject(const Obj: TBatchedObject);
    {}
    //Warning:checks collision only for a point, not rect!
    function CheckCollision(const ax, ay: Single): Boolean;
    function CheckCollision(const Rect: TFloatRectangle): Boolean;
    { Call-back to rebuild colliders in the world (in case some object's collider has changed) }
    procedure RebuildColliders;
    { Update objects in this world }
    procedure Update(const SecondsPassed: Single);
    { Queue batched render of the World }
    procedure QueueRender;
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;
  
type
  { A rendering queue that stacks and renders batched images }
  TRenderQueue = class(TObject)
  strict private
    { Comparer to sort object list }
    FComparer: TBatchedComparer;
    { List of objects to be rendered }
    FObjectList: TObjectList;
    { Sort FObjectList }
    procedure SortObjectList;
  public
    { Add an object to the render queue }
    procedure Add(const aBatchedObject: TBatchedObject);
    { Actually render the queue }
    procedure Render;
  public
    constructor Create; //override;
    destructor Destroy; override;
  end;
  
var
  { Queue that stacks and renders images }
  RenderQueue: TRenderQueue;
  { Current character controlled by the Player }
  CurrentCharacter: TPlayerCharacter;
  { Current world }
  World: TWorld;

function TBatchedObject.isOnScreen: Boolean;
begin
  UpdateScreenRect;
  //GameScreenRect := FloatRectangle(GameScreenLeft, GameScreenBottom, GameScreenWidth, GameScreenHeight);
  Result := ScreenRect.Collides(GameScreenRect);
end;

procedure TBatchedObject.Update(const SecondsPassed: Single);
begin
  FTime += SecondsPassed;
end;

function TBatchedObject.IfPointCollides(const ax, ay): Boolean;
begin
  Result := (ax >= x) and (ax <= x + Width) and
    (ay >= y) and (ay <= x + Width);  
end;

procedure TBatchedObject.UpdateCollisionRect; inline;
begin
  CollisionRect := FloatRectangle(x, y, x + Width, y + Height);
end;

procedure TBatchedObject.UpdateScreenRect; inline;
begin
  ScreenRect := FloatRectangle(x - GameScreenStartX, y - GameScreenStartY,
    x + Width - GaameScreenStartX, y + Height - GameScreenStartY); // todo: scale!
end;

function TBatchedObject.IfCollides(const aRect: TFloatRectangle): Boolean;
begin
  if FCollisionNeedsUpdate then
    UpdateCollisionRect;
  Result := CollisionRect.Collides(aRect);
  if Result and Assigned(onCollision) then
    onCollision;
end;

procedure TBatchedObject.SetCollides(const aValue: Boolean);
begin
  if FCollides <> aValue then
  begin
    FCollides := aValue;
    //if collision is changed after Creation or is true (to avoid rebuilding after creating missiles?)
    World.QueueRebuildColliders;
  end;
end;

procedure TBatchedObject.GetWidth: Single;
begin
  Result := ImageRect.Width;
end;

procedure GetHeight: Single;
begin
  Result := ImageRect.Height;
end;

procedure TBatchedObject.QueueRender;
begin
  if OnScreen then
  begin
    //UpdateScreenRect; <----- already called in OnScreen;
    //normally Image.ImageRect should be set only once during creation unless object is animated;
    ImageQueue.Add(Self);
end;

procedure TAnimatedObject.Update(const SecondsPassed: Single);
begin
  inherited Update(SecondsPassed);
  if FTime > Animations[CurrentAnimation].Duration then
    //end or loop animation
    FTime -= Animations[CurrentAnimation].Duration;
end;

function TAnimatedObject.CurrentAnimationFrame: TFloatRectangle;
begin
  CurrentFrame := Round(Animations[CurrentAnimation].TotalFrames * FTime /Animations[CurrentAnimation].Duration;
  Result := Animations[CurrentAnimation].Rect[CurrentFrame];
end;

procedure TAnimatedObject.QueueRender;
begin
  //inherited <---------- for efficiency reasons we just rewrite the code
  if OnScreen then
  begin
    ImageRect := CurrentAnimationFrame;
    ScreenRect := FloatRectangle(x, y, x + Width, y + Height);
    ImageQueue.Add(Image);
  end;
end;

function TDynamicObject.StandingOnTheGround: Boolean;
begin
  Result := World.CheckCollision(x + Width / 2, vy - 0.5); // this looks enough to check only feet collision
end;

procedure TDynamicObject.Update(const SecondsPassed: Single);
var
  NewX, NewY: Single;
  NewVX, NEWVY:Single;
  function GetNewXY: Boolean;
  begin
    NewX := x + NewVX;
    NewY := y + NewVY;
    Result := World.CheckCollision(NewX, NewY);
  end;
begin
  inherited Update(SecondsPassed);
  NewVX := vx *SecondsPassed ;
  NewVY := vy - GravityAcceleration * SecondsPassed;
  //while not GetNewXY do //will cause freezes if current xy aren't accessible anymore, e.g. due to a moving block
  GetNewXY;
  if not World.CheckCollision(NextRect(NewX, NewY)) then
  begin
    //optimize NextRect by reusing collisionrect copy (make it nested)
    if World.CheckCollision(NextRect(x, NewY)) then
      NewVX := 0
    else
    if World.CheckCollision(NextRect(NewX, y)) then
      NewVY := 0
    else
    begin
      NewVX := 0;
      NewVY := 0;
    end;
  end;
  
  //move these animations checks to Update to override user input animations
  if NewVY <= GravityAcceleration * SecondsPassed then
    ForceAnimation(atFallDown);
  else
  if NewVY >= JumpAcceleration * SecondsPassed then
    ForceAnimation(atFallDown);

  //this is a stop, do a stop animation
  if (NewVY = 0) and (vy < 0) then
    ForceAnimation(atLand);
  
  if StandingOnTheGround then
    NewVX := FrictionCoefficient * NewVX;  
  x := NewX;
  y := NewY;
  vx := NewVX;
  vy := NewVY;
end;

function TDynamicObject.NextRect(const avx, avy: Single): TFloatRectangle;
begin
  if FNeedsUpdateCollisionRect then
    UpdateCollisionRect;
  Result := CollisionRect;
  Result.x := Result.x + avx;
  Result.y := Result.y + avy;
end;

procedure.TDynamicObject.Jump;
begin
  if StandingOnTheGround then //Double/tripple jump?
    vy += JumpAcceleration;
    ForceAnimation(atJump);
  end;  
end;

procedure TDynamicObject.MoveMe(const DeltaX: Integer);
begin
  vx := vx + MoveSpeed.DeltaX;
  if StandingOnTheGround then
    ForceAnimation(atRun); //should automatically flip image based on vx
end;

procedure TSmartObject.PathFinder;
var
  FTester: TDynamicCreature;
begin
  FTester := TDynamicCreature.Create; //todo: cache it!
  //the idea is to guess a set of actions to get from current(x,y) to target(x,y)
  FTester.CopyFrom(Self) //<----- this is the Root state;
  ...
end;

procedure TCreature.HitMe(const Damage: Single);
begin
  if HP > Damage then
    HP -= Damage
  else
  begin
    HP := -1;
    Die;
  end;
end;

procedure TCreature.Die;
begin
  ForceAnimation(atDie);
  Collides := false;
  //disable creature's AI
end;

{----------------------------------------------------------------------------}

procedure ImageQueueComparer(Image1, Image2: TBatchedObject): Integer;
begin
  if Image1.zIndex > Image2.zIndex then
  {$Warning Test the render order here!}
    Result := 1
  else
  if Image1.zIndex < Image2.zIndex then
    Result := -1
  else
    if Integer(Image1.Image) > Integer(Image2.Image) then
      Result := 1
    else
    if Integer(Image1.Image) < Integer(Image2.Image) then
      Result := -1
    else
      Result := 0;
end;

procedure TImageQueue.SortObjectList;
begin
   FObjectList.Sort(FComparer);
end;

procedure TImageQueue.Render;
var
  ScreenRects, ImageRects: TDynamicFloatRectangleAray;
  Count: Integer;
  procedure RenderLastImage; inline;
  begin
      if LastImage <> nil then
        LastImage.Image.Draw(TFloatRectangle(ScreenRects),
          TFloatRectangle(ImageRects), Count);  
  end;
begin
  SortObjectList; //this guarantees that identical images with same zIndex are sequential
  LastImage := nil;
  for i := 0 to Pred(FObjectList.Count) do
  begin
    if FObjectList.Image <> LastImage then
    begin
      RenderLastImage;
      Count := 0;
       //we don't need finalize here, as the arrays will just be reused
      LastImage := FObjectList.Image;
    end;
  //add image to batched render queue
  ScreenRects[Count] := FObjectList.ScreenRect;
  ImageRects[Count] := FObjectList.ImageRect;
  inc(Count);
  end;
  RenderLastImage;
  FObjectList.Clear;
end;

procedure TImageQueue.Add(const aBatchedObject: TBatchedObject);
begin
  FObjectList.Add(aBatchedObject);
end;

constructor TImageQueue.Create;
begin
  //inherited <------- parent is empty;
  FObjectList := TBatchedObjectList.Create(false); //doesn't own children
  FComparer := ...
end;

destructor TImageQueue.Destroy;
begin
  FObjectList.Free;
  inherited Destroy;
end;

{--------------------------------------------------------------------------}

procedure doRender(Container:TUIcontainer);
begin
  World.QueueRender;
  //actually render all images on screen
  ImageQueue.Render;
end;

procedure doPress(Container: TUIContainer; Event: TPressReleaseEvent);
const
  ControlUp = KeyW;
  ControlLeft = KeyA;
  ControlRight = KeyD;
begin
  if Event.EventType = itKeyboard then
    case Event.Key of
      ControlUp: CurrentCharacter.Jump;
      ControlLeft: CurrentCharacter.Move(-1);
      ControlRight: CurrentCharacter.Move(+1);
    end;  
end;

procedure SpawnCreatureByName(const aName: String);
begin
  case aName of
    'Helia': ...;
    'Fengar': ...;
  end;
end;

{----------------------------------------------------------------}

function TWorld.isValid(const ax, ay: Integer): Boolean; inline;
begin
  Result := (ax >=0) and (ax <= MapSizeX * MapScaleX) and
    (ay >= 0) and (ay <= MapSizeY * MapScaleY);
end;

function TWorld.Map(const ax, ay: Integer): Integer; inline;
begin
  if isValid(ax, ay) then
    Result := FMap[ax, ay]
  else
    Result := -1;
end;

function TWorld.MapForeground(const ax, ay: Integer): Integer; inline;
begin
  if isValid(ax, ay) then
    Result := FMapForeground[ax, ay]
  else
    Result := -1;
end;

function TWorld.MapBackground(const ax, ay: Integer): Integer; inline;
begin
  if isValid(ax, ay) then
    Result := FMapBackground[ax, ay]
  else
    Result := -1;
end;

function TWorld.IsPassable(const ax, ay: Integer): Boolean; inline;
begin
  Result := Map[ax, ay] = 0;
end;

function TWorld.CheckCollision(const ax, ay: Single): Boolean;
var
  Obj: TBatchedObject;
begin
  if IsPassable(Trunc(ax), Trunc(ay)) then
  begin
    Result := true;
    for Obj in FCollidersList do
      if Obj.IfPointInside(ax, ay) then
      begin
        Result := false;
        Exit;
      end;
  end else
    Result := false;
end;

function TWorld.CheckCollision(const aRect: TFloatRectangle): Boolean;
var
  Obj: TBatchedObject;
begin
  Result := true;
  for ix := Trunc(aRect.Left) to Trunc(aRect.Right) do
    for iy := Trunc(aRect.Bottom) to Trunc(aRect.Top) do
      if not IsPassable(ix, iy) then
      begin
        Result := false;
        Exit;
      end;
  for Obj in FCollidersList do
    if Obj.IfCollides(aRect) then
    begin
      Result := false;
      Exit;
    end;
end;

function TWorld.Add(const Obj: TBatchedObject);
begin
  FObjectList.Add(Obj);
end;

procedure TWorld.QueueRenderMap;
var
  ix, iy: Integer;
begin
  for ix := Trunc(GameScreenStartX / MapScaleX) to Trunc(GameScreenStartX + GameWidth / MapScaleX) + 1 do
    for iy := Trunc(GameScreenStartY / MapScaleY) to Trunc(GameScreenStartY + GameHeight / MapScaleY) + 1 do
    begin
      if MapBackground(ix, iy) > 0 then
        MapAtlas.QueueRender(MapAtlasRender[Map(ix, iy)], -100);
      if Map(ix, iy) > 0 then
        MapAtlas.QueueRender(MapAtlasRender[Map(ix, iy)], 0);
      if MapForeground(ix, iy) > 0 then
        MapAtlas.QueueRender(MapAtlasRender[Map(ix, iy)], 100);
    end;
end

procedure TWorld.QueueRender;
var
  Obj: TBatchedObject;
begin
  //Render background with z = -10000;
  //Render sea backround with z = -1000;
  //Render sea foregrond with z = 1000;
  QueueRenderMap;
  //prepare objects for rendering
  for Obj in FObjectsList do
    Obj.QueueRender;
  end;
end;

procedure TWorld.RebuildColliders;
var
  Obj: TBatchedObject;
begin
  FCollidersList.Clear;
  for Obj in FObjectsList do
    if Obj.Collides then
      FCollidersList.Add(Obj);
  FNeedRebuildColliders := false;
end;
  

procedure TWorld.QueueRebuildColliders;
begin
  FNeedRebuildColliders := true;
end;

procedure TWorld.Update(const SecondsPassed: Single);
begin
  //rebuild colliders list
  if FNeedRebuildColliders then
    RebuildColliders;
  //update objects on map
  for Obj in FObjectsList do
    Obj.Update(Window.FPS.SecondsPassed);   
end;

constructor TWorld.Create;
begin
  //inherited <---------- Parent is empty
  FObjectList := TBatchedObjectList.Create(true); //?doesn't own children? Or owns everyone except player characters?
  FCollidersList := TBatchedObjectList.Create(false); //it's only a reference list, doesn't own objects
  FNeedRebuildColliders := true;
end;

destructor TWorld.Destroy;
begin
  FObjectList.Free;
  FCollidersList.Free;
  inherited Destroy;
end;


